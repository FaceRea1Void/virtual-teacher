'use strict';

function checkPassword(form) {
    if (form.password.value !== form.password_confirm.value)
    {
        $('.password-error').html('<span class="text-danger align-middle">Passwords do not match</span>');
        return false;
    } else {
        return true;
    }
}