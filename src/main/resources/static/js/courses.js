'use strict';

loadCourses();

$(document).ready(function () {
    $(document).on('click', 'a.page-link', function (event) {
        if (!$(this).parent().hasClass('active')) {
            loadCourses($(this).data('value'));
            event.preventDefault();
        }
    });
});

function clearFilters(event) {
    $('#title').val('').trigger('change');
    $('#teacher').val('').trigger('change');
    $('#category').val('').trigger('change');
    $('#orderBy').val('').trigger('change');
    loadCourses();
}

function loadCourses(page) {
    // console.log('test');
    let title = $('#title').val();
    let teacher = $('#teacher option:selected').val();
    let category = $('#category option:selected').val();
    let orderBy = $('#orderBy option:selected').val();

    if (page === undefined) {
        page = 0;
    } else {
        page = Number(page) - 1;
    }

    let $url = generateUrl(title, teacher, category, page, orderBy);
    // console.log($url);

    $.ajax({
        url: $url,
        success: function (response) {
            $('.filteredCourses').html('');
            if (response.totalElements === 0) {
                $('.filteredCourses').append(`<h1> No courses found by searched criteria</h1>`);
                $('.pagination').html("");

            } else {
                $('.pagination').html('');
                $('.pagination').append(`<li class="page-item prev"><a data-value="${response.pageable.pageNumber}" class="page-link" href="" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a></li>`);

                for (let i = 1; i <= response.totalPages; i++) {
                    $('.pagination').append(`<li class="page-item page-item-${i}"><a data-value="${i}" class="page-link" href="#coursesByCategory">${i}</a></li>`);
                    if (i === response.pageable.pageNumber + 1) {
                        $('.page-item-' + String(i)).addClass('active');
                    }
                }

                $('.pagination').append(`<li class="page-item next"><a data-value="${response.pageable.pageNumber + 2}" class="page-link" href="" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a></li>`);

                if (response.pageable.pageNumber === 0) {
                    $('.prev').addClass('disabled');
                }
                if (response.pageable.pageNumber + 1 === response.totalPages) {
                    $('.next').addClass('disabled');
                }

                // console.log(response);
                $.each(response.content, function (i) {
                    let str = (response.content[i].category.name).replace(/\s/g, '').toLowerCase();
                    let image;
                    if (response.content[i].image !== null) {
                        image = 'data:image/png;base64,' + response.content[i].image.image;
                    } else {
                        image = '/img/image-placeholder.jpg';
                    }
                    $('.filteredCourses')
                        .append(`
                    <!-- course -->
            <div class="mix col-lg-3 col-md-4 col-sm-6 ${str}">
            <a class="course-link" href="/courses/${response.content[i].id}">
                <div class="course-item">
                    <div class="course-thumb set-course-bg" data-setbg="${image}">
                    <div class="rating">Rating: ${response.content[i].avgRating}</div>
                    </div>
                    <div class="course-info">
                        <div class="course-text">
                            <h5>${response.content[i].title}</h5>
                            <p>${response.content[i].category.name}</p>
                            <div class="description" title="${response.content[i].description}">${response.content[i].description}</div>
                        </div>
                        <div class="course-author">
                            <p>${response.content[i].teacher}</p>
                        </div>
                    </div>
                </div>
                </a>
                </div>`);

                    /*------------------
                    Background Set
                    --------------------*/
                    $('.course-thumb.set-course-bg:last').each(function () {
                        let bg = $(this).data('setbg');
                        // console.log(bg);
                        $(this).css('background-image', 'url(' + bg + ')');
                    });
                })
            }
        }
    })

    /*------------------
       Gallery item
       --------------------*/
    if ($('.course-items-area').length > 0) {
        // console.log($('.course-items-area').length);
        let containerEl = document.querySelector('.course-items-area');
        console.log(containerEl);
        let mixer = mixitup(containerEl, {
            behavior: {
                liveSort: true
            }
        });
    }

}

function generateUrl(title, teacher, category, page, orderBy) {
    let url = "/api/courses?";

    if (title !== "") {
        url = url + "title=" + title + "&";
    }

    if (teacher !== "") {
        url = url + "teacher=" + teacher + "&";
    }
    if (category !== "") {
        url = url + "category=" + category + "&";
    }
    if (orderBy !== "") {
        url = url + "sort=" + getSortExpression(orderBy) + "&";
    }

    url = url + "page=" + page;
    return url;
}

function getSortExpression(orderBy) {
    let orderMap = {
        'ra': 'avgRating,asc',
        'rd': 'avgRating,desc',
        'ta': 'title,asc',
        'td': 'title,desc'
    };
    return orderMap[orderBy];
}
