package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseImage;
import com.company.virtualteacher.repositories.CourseImageRepository;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.services.interfaces.CourseImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseImageServiceImpl implements CourseImageService {
    private CourseImageRepository courseImageRepository;
    private CourseRepository courseRepository;

    @Autowired
    public CourseImageServiceImpl(CourseImageRepository courseImageRepository, CourseRepository courseRepository) {
        this.courseImageRepository = courseImageRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public void store(CourseImage courseImage, String teacher) {
        Course course = courseRepository.findByIdAndStatus(courseImage.getCourseId(), CourseStatus.STATUS_DRAFT);
        if (course == null) {
            throw new IllegalArgumentException(String.format("Course with id %s is not a draft.", courseImage.getCourseId()));
        }
        if (!course.getTeacher().equals(teacher)) {
            throw new IllegalArgumentException(String.format("Course with id %s does not belong to %s.", course.getId(), teacher));
        }
        courseImageRepository.save(courseImage);
        courseRepository.updateImageId(courseImage.getCourseId(), courseImage.getId());
    }

    @Override
    public CourseImage getByCourseId(int courseId) {
        return courseImageRepository.findByCourseId(courseId);
    }
}
