package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.Lecture;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.services.interfaces.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectureServiceImpl implements LectureService {
    private LectureRepository lectureRepository;
    private CourseRepository courseRepository;

    @Autowired
    public LectureServiceImpl(LectureRepository lectureRepository, CourseRepository courseRepository) {
        this.lectureRepository = lectureRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public Lecture getByTitle(String title) {
        return lectureRepository.findLectureByTitle(title);
    }

    @Override
    public Lecture getById(int id) {
        return lectureRepository.findById(id).orElseThrow(() ->
                new IllegalArgumentException(String.format("Lecture with id %d not found.", id)));
    }

    @Override
    public void create(Lecture lecture, String username) {
        checkCourse(lecture.getCourseId(), username);
        checkLectureTitle(lecture);

        List<Lecture> courseLectures = lectureRepository.findAllByCourseIdAndIsActiveTrue(lecture.getCourseId());
        if (!courseLectures.isEmpty()) {
            Lecture prevLecture = courseLectures.get(courseLectures.size() - 1);
            lecture.setPrevLectureId(prevLecture.getId());
            lectureRepository.save(lecture);
            prevLecture.setNextLectureId(lecture.getId());
            lectureRepository.save(prevLecture);
        }
        else {
            lectureRepository.save(lecture);
        }
    }

    @Override
    public void update(Lecture lecture, int lectureId, String username) {
        if (lecture.getId() != lectureId) {
            throw new IllegalArgumentException(String.format("Lecture with id %d not found.", lecture.getId()));
        }
        checkCourse(lecture.getCourseId(), username);
        checkLectureTitle(lecture);

        Lecture existingLecture = lectureRepository.findById(lecture.getId())
                .orElseThrow(() -> new IllegalArgumentException
                        (String.format("Lecture with id %d not found.", lecture.getId())));
        lecture.setId(existingLecture.getId());
        lecture.setAssignmentId(existingLecture.getAssignmentId());
        lecture.setPrevLectureId(existingLecture.getPrevLectureId());
        lecture.setNextLectureId(existingLecture.getNextLectureId());
        lectureRepository.save(lecture);
    }

    @Override
    public int countLecturesByCourseId(int courseId) {
        return lectureRepository.countAllByCourseId(courseId);
    }

    @Override
    public List<Lecture> getLectureListByCourseId(int courseId) {
        return lectureRepository.findAllByCourseIdAndIsActiveTrue(courseId);
    }

    private void checkCourse(int courseId, String username) {
        Course course = courseRepository.findByIdAndStatus(courseId, CourseStatus.STATUS_DRAFT);
        if (course == null) {
            throw new IllegalArgumentException(String.format("Course with id %d is not a draft.", courseId));
        }
        if (!course.getTeacher().equals(username)) {
            throw new IllegalArgumentException
                    (String.format("Course with id %d does not belong to %s.", courseId, username));
        }
    }

    private void checkLectureTitle(Lecture lecture) {
        if (lectureRepository.findAllByCourseIdAndIsActiveTrue(lecture.getCourseId()).stream()
                .filter(l -> l.getId() != lecture.getId())
                .anyMatch(l -> l.getTitle().equals(lecture.getTitle()))) {
            throw new IllegalArgumentException
                    (String.format("Lecture with title '%s' already exists for this course.", lecture.getTitle()));
        }
    }
}
