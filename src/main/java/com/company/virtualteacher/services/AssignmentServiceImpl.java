package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Assignment;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.Lecture;
import com.company.virtualteacher.repositories.AssignmentRepository;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.repositories.UserLectureRepository;
import com.company.virtualteacher.services.interfaces.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class AssignmentServiceImpl implements AssignmentService {
    private static final String LECTURE_DOES_NOT_EXIST = "Lecture with id %d does not exist.";
    private static final String ASSIGNMENT_NOT_FOUND = "Assignment with id %d was not found.";
    private static final String USERNAME_DOES_NOT_MATCH = "Username does not match.";
    private static final String COURSE_NOT_A_DRAFT = "Course with id %d is not a draft.";

    private AssignmentRepository assignmentRepository;
    private LectureRepository lectureRepository;
    private CourseRepository courseRepository;
    private UserLectureRepository userLectureRepository;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository, LectureRepository lectureRepository, CourseRepository courseRepository, UserLectureRepository userLectureRepository) {
        this.assignmentRepository = assignmentRepository;
        this.lectureRepository = lectureRepository;
        this.courseRepository = courseRepository;
        this.userLectureRepository = userLectureRepository;
    }

    @Override
    public Assignment store(Assignment assignment, String username) {
        checkLectureAdnCourse(assignment.getLectureId(), username);

        Lecture lecture = lectureRepository.findById(assignment.getLectureId())
                .orElseThrow(() -> new IllegalArgumentException(String.format(LECTURE_DOES_NOT_EXIST, assignment.getLectureId())));

        Assignment currentAssignment = assignmentRepository.findByLectureId(assignment.getLectureId());
        if (currentAssignment != null) {
            assignment.setId(currentAssignment.getId());
        }

        assignmentRepository.save(assignment);
        lecture.setAssignmentId(assignment.getId());
        lectureRepository.save(lecture);
        return assignment;
    }

    @Override
    public Assignment get(int assignmentId, int lectureId, String username) {
        Assignment assignment = assignmentRepository.findById(assignmentId)
                .orElseThrow(() -> new IllegalArgumentException(String.format(ASSIGNMENT_NOT_FOUND, assignmentId)));

        if (assignment.getLectureId() != lectureId) {
            throw new IllegalArgumentException(String.format(ASSIGNMENT_NOT_FOUND, assignmentId));
        }
        if (userLectureRepository.findByLectureIdAndStudentAndAvailable(lectureId, username, true) == null) {
            throw new IllegalArgumentException(String.format(ASSIGNMENT_NOT_FOUND, assignmentId));
        }

        return assignment;
    }

    @Override
    public String getFileName(int assignmentId, int lectureId, String username, String authority) {
        Assignment assignment = assignmentRepository.findById(assignmentId)
                .orElseThrow(() -> new IllegalArgumentException(String.format(ASSIGNMENT_NOT_FOUND, assignmentId)));

        if (assignment.getLectureId() != lectureId) {
            throw new IllegalArgumentException(String.format(ASSIGNMENT_NOT_FOUND, assignmentId));
        }

        if (authority.equals("ROLE_STUDENT") && authority != null) {
            if (userLectureRepository.findByLectureIdAndStudentAndAvailable(lectureId, username, true) == null) {
                throw new IllegalArgumentException(String.format(ASSIGNMENT_NOT_FOUND, assignmentId));
            }
        }
        else if (authority.equals("ROLE_TEACHER") && authority != null) {
            checkLectureAdnCourse(lectureId, username);
        }

        return assignment.getFileName();
    }

    private void checkLectureAdnCourse(int lectureId, String username) {
        Lecture lecture = lectureRepository.findById(lectureId)
                .orElseThrow(() -> new IllegalArgumentException(String.format(LECTURE_DOES_NOT_EXIST, lectureId)));

        Course course = courseRepository.findByIdAndStatus(lecture.getCourseId(), CourseStatus.STATUS_DRAFT);
        if (course == null) {
            throw new IllegalArgumentException(String.format(COURSE_NOT_A_DRAFT, lecture.getCourseId()));
        }
        if (!course.getTeacher().equals(username)) {
            throw new IllegalArgumentException(USERNAME_DOES_NOT_MATCH);
        }
    }
}
