package com.company.virtualteacher.services;

import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.interfaces.EmailNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailNotificationServiceImpl implements EmailNotificationService {
    private JavaMailSender javaMailSender;
    private UserRepository userRepository;

    @Autowired
    public EmailNotificationServiceImpl(JavaMailSender javaMailSender, UserRepository userRepository) {
        this.javaMailSender = javaMailSender;
        this.userRepository = userRepository;
    }

    @Override
    public void sendEmail(String username) throws MessagingException {
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        String teacherEmail = userRepository.findUserByUsername(username).getEmail();

        if(teacherEmail == null){
            throw new IllegalArgumentException("User doesn't set an email.");
        }

        helper.setTo(teacherEmail);

        helper.setSubject("Digital Uni Teacher request approval");
        String message =
                "<div class=\"\" style=\"margin:0 auto;max-width:450px; background-color:#f7f7f7\">\n" +
                "    <div class=\"\" style=\"margin-bottom:22px;text-align:center;\">\n" +
                "        <div class=\"\" style=\"background-color:#f7f7f7;margin:0 auto;text-align:center;\">\n" +
                "            <img height=\"122\" src=\"https://drd50mzzs58bu.cloudfront.net/static/lib/eshares/mailer/confetti.gif\"\n" +
                "                 style=\"display:block;margin:0 auto;width:100.0%;\" width=\"448\">\n" +
                "        </div>\n" +
                "        <div class=\"\" style=\"background-color:#f7f7f7;color:#41BC3F;margin:0 auto;padding:28px;\">\n" +
                "            <h1 style=\"color:#41BC3F;font-size:21px;font-weight:500;line-height:1.25;margin-bottom:28px;text-align:center;width:100.0%;\">\n" +
                "                Congratulations! We are all really excited to welcome you to our team!</h1>\n" +
                "        </div>\n" +
                "        <div class=\"\" style=\"margin:0 auto;padding:28px;text-align:center;\">\n" +
                "            <p style=\"margin:0 0 21px;color:#41BC3F;font-size:11px;margin-bottom:14px;\"> The contents of this e-mail\n" +
                "                message and any attachments are confidential and may be legally privileged. If you are not the intended\n" +
                "                recipient, notify the sender by replying to this e-mail and delete this message and its attachments, if\n" +
                "                any.</p>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n";

        helper.setText(message, true);

        javaMailSender.send(msg);
    }
}
