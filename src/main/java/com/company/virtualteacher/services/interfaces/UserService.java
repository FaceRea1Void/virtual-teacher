package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.User;

public interface UserService {
    void create(org.springframework.security.core.userdetails.User user, String resume, String email);

    User getDetails(String username);

    void changeRole(org.springframework.security.core.userdetails.User user);


    void delete(org.springframework.security.core.userdetails.UserDetails user);

    void update(User user);
}
