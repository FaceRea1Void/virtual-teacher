package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.Assignment;

public interface AssignmentService {
    Assignment store(Assignment assignment, String username);

    Assignment get(int assignmentId, int lectureId, String username);

    String getFileName(int assignmentId, int lectureId, String username, String authority);
}
