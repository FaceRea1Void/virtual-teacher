package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.CourseRating;

public interface CourseRatingService {
    void add(CourseRating courseRating, String username);

    int getUserRating(int courseId, String username, String loggedUser);
}
