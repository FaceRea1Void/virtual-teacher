package com.company.virtualteacher.services.interfaces;

import com.company.virtualteacher.models.Category;

import java.util.List;

public interface CategoryService {
    void create(Category category, String authority);

    void update(Category category);

    Category getById(int categoryId);

    List<Category> getCategories();
}
