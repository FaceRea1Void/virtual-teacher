package com.company.virtualteacher.services;

import com.company.virtualteacher.models.PendingApprovals;
import com.company.virtualteacher.repositories.PendingApprovalRepository;
import com.company.virtualteacher.services.interfaces.PendingApprovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PendingApprovalServiceImpl implements PendingApprovalService {
    private PendingApprovalRepository pendingApprovalsRepository;

    @Autowired
    public PendingApprovalServiceImpl(PendingApprovalRepository pendingApprovalsRepository) {
        this.pendingApprovalsRepository = pendingApprovalsRepository;
    }

    @Override
    public void approve(String username) {
        List<PendingApprovals> approvals = pendingApprovalsRepository.findAllByUsername(username);
        if (approvals.isEmpty()) {
            throw new IllegalArgumentException(String.format("No waiting request for username %s", username));
        }

        for (PendingApprovals request : approvals) {
            pendingApprovalsRepository.delete(request);
        }
    }

    @Override
    public Page<PendingApprovals> getPendingApprovals(String username, Pageable pageable) {
        if (username == null) {
            username = "";
        }

        return pendingApprovalsRepository.findAllByUsernameContaining(username, pageable);
    }
}
