package com.company.virtualteacher.services;

import com.company.virtualteacher.models.PendingApprovals;
import com.company.virtualteacher.models.User;
import com.company.virtualteacher.repositories.PendingApprovalRepository;
import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private UserDetailsManager userDetailsManager;
    private PendingApprovalRepository pendingApprovalsRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserDetailsManager userDetailsManager, PendingApprovalRepository pendingApprovalsRepository) {
        this.userRepository = userRepository;
        this.userDetailsManager = userDetailsManager;
        this.pendingApprovalsRepository = pendingApprovalsRepository;
    }

    @Override
    public void create(org.springframework.security.core.userdetails.User user, String resume, String email) {
        boolean existingUser = userDetailsManager.userExists(user.getUsername());
        if (existingUser) {
            throw new IllegalArgumentException(String.format("Username %s already exists.", user.getUsername()));
        }

        userDetailsManager.createUser(user);
        if (resume != null) {
            PendingApprovals newApproval = new PendingApprovals(user.getUsername(), resume);
            pendingApprovalsRepository.save(newApproval);
        }

        User createdUser = userRepository.findUserByUsername(user.getUsername());
        createdUser.setEmail(email);
        userRepository.save(createdUser);
    }

    @Override
    public User getDetails(String username) {
        User user = userRepository.findUserByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException("User not found.");
        }
        return user;
    }

    @Override
    public void changeRole(org.springframework.security.core.userdetails.User user) {
        userDetailsManager.updateUser(user);
    }

    @Override
    public void update(User user) {
        User currentUser = userRepository.findUserByUsername(user.getUsername());
        if (currentUser == null) {
            throw new IllegalArgumentException("User not found.");
        }
        userRepository.updateDetails(user.getUsername(), user.getEmail(), user.getFirstName(), user.getMiddleName(), user.getLastName());
    }

    @Override
    public void delete(UserDetails user) {
        // test errors from DB
        User userDetails = userRepository.findUserByUsername(user.getUsername());
        if (userDetails == null) {
            throw new IllegalArgumentException("User not found.");
        }

        userDetails.setUsername(user.getUsername());
        userDetails.setEmail(user.getUsername()+"@deleted.user");
        userDetails.setEnabled(false);
        userDetailsManager.updateUser(user);
        userRepository.save(userDetails);
    }
}
