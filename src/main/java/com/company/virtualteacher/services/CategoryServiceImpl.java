package com.company.virtualteacher.services;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.repositories.CategoryRepository;
import com.company.virtualteacher.services.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private static final String UNAUTHORIZED_REQUEST = "Unauthorized request.";
    private static final String CATEGORY_ALREADY_EXISTS = "Category with name %s already exists.";
    private static final String CATEGORY_DOES_NOT_EXIST = "Category with id %d doesn't exist.";

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository) {
        this.categoryRepository = repository;
    }

    @Override
    public void create(Category category, String authority) {
        if (!authority.equals("ROLE_TEACHER") && authority != null) {
            throw new IllegalArgumentException(UNAUTHORIZED_REQUEST);
        }

        if (categoryRepository.findCategoryByName(category.getName()) != null) {
            throw new IllegalArgumentException(String.format(CATEGORY_ALREADY_EXISTS, category.getName()));
        }

        categoryRepository.save(category);
    }

    @Override
    public void update(Category category) {
        if (!categoryRepository.findById(category.getId()).isPresent()) {
            throw new IllegalArgumentException(String.format(CATEGORY_DOES_NOT_EXIST, category.getId()));
        }

        categoryRepository.save(category);
    }

    @Override
    public Category getById(int categoryId) {
        return categoryRepository.findById(categoryId).orElseThrow(() ->
                new IllegalArgumentException(String.format(CATEGORY_DOES_NOT_EXIST, categoryId)));
    }

    @Override
    public List<Category> getCategories() {
        return categoryRepository.findAllByOrderByNameAsc();
    }
}
