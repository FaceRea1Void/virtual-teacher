package com.company.virtualteacher.services;

import com.company.virtualteacher.models.UserLecture;
import com.company.virtualteacher.repositories.UserLectureRepository;
import com.company.virtualteacher.services.interfaces.UserLecturesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserLecturesServiceImpl implements UserLecturesService {
    private static final String LECTURE_IS_NOT_AVAILABLE_FOR_STUDENT = "This lecture is not available for student %s";
    private static final String USERNAME_DOES_NOT_MATCH = "Username does not match.";

    private UserLectureRepository userLectureRepository;

    @Autowired
    public UserLecturesServiceImpl(UserLectureRepository userLectureRepository) {
        this.userLectureRepository = userLectureRepository;
    }

    @Override
    public List<UserLecture> getUserLectures(int courseId, String student, String loggedUser) {
        checkUsername(student, loggedUser);
        return userLectureRepository.findAllByCourseIdAndStudentOrderByLectureId(courseId, student);
    }

    @Override
    public boolean isLectureAvailable(int lectureId, String student) {
        return userLectureRepository.findByLectureIdAndStudentAndAvailable(lectureId, student, true) != null;
    }

    @Override
    public void setLectureAvailable(int lectureId, String student, String loggedUser) {
        checkUsername(student, loggedUser);
        UserLecture userLecture = userLectureRepository.findByLectureIdAndStudentAndAvailable(lectureId, student,false);
        if (userLecture == null) {
            throw new IllegalArgumentException(String.format(LECTURE_IS_NOT_AVAILABLE_FOR_STUDENT, student));
        }
        if (userLecture.getPrevLectureId() != null) {
            UserLecture prevUserLecture = userLectureRepository.findByLectureIdAndStudentAndAvailable(userLecture.getPrevLectureId(), student, true);
            if (prevUserLecture != null && !prevUserLecture.isAvailable()) {
                throw new IllegalArgumentException(String.format(LECTURE_IS_NOT_AVAILABLE_FOR_STUDENT, student));
            }
        }

        userLecture.setAvailable(true);
        userLectureRepository.save(userLecture);
    }

    @Override
    public UserLecture getByLectureIdAndStudent(int lectureId, String student, String loggedUser) {
        checkUsername(student, loggedUser);
        UserLecture userLecture = userLectureRepository.findByLectureIdAndStudentAndAvailable(lectureId, student,true);
        if (userLecture == null) {
            throw new IllegalArgumentException(String.format(LECTURE_IS_NOT_AVAILABLE_FOR_STUDENT, student));
        }
        return userLecture;
    }

    @Override
    public void setVideoWatched(int lectureId, String student, String loggedUser) {
        checkUsername(student, loggedUser);
        UserLecture userLecture = userLectureRepository.findByLectureIdAndStudentAndAvailable(lectureId, student,true);
        if (userLecture == null) {
            throw new IllegalArgumentException(String.format(LECTURE_IS_NOT_AVAILABLE_FOR_STUDENT, student));
        }
        userLecture.setVideoWatched(true);
        userLectureRepository.save(userLecture);
    }

    @Override
    public int getProgressByCourseId(int courseId, String student, String loggedUser) {
        checkUsername(student, loggedUser);
        List<UserLecture> userLectures = userLectureRepository
                .findAllByCourseIdAndStudentOrderByLectureId(courseId, student);
        if (userLectures.isEmpty()) {
            return 0;
        }

        double progressCount = userLectures.stream().reduce(0.0, (partial, ul) -> {
            if (ul.isVideoWatched() && ul.isHomeworkSubmitted()) {
                return partial + 1.0;
            }
            else if (ul.isVideoWatched() || ul.isHomeworkSubmitted()) {
                return partial + 0.5;
            }
            else {
                return partial +  0.0;
            }
        }, Double::sum);

        return (int) Math.round((progressCount / userLectures.size()) * 100);
    }

    private void checkUsername(String username, String loggedUser) {
        if (!loggedUser.equals(username)) {
            throw new IllegalArgumentException(USERNAME_DOES_NOT_MATCH);
        }
    }
}
