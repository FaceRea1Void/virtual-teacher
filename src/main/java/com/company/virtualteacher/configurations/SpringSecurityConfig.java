package com.company.virtualteacher.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    private String dbUrl;
    private String username;
    private String password;

    @Autowired
    public SpringSecurityConfig(Environment env) {
        dbUrl = env.getProperty("database.url");
        username = env.getProperty("database.username");
        password = env.getProperty("database.password");
    }

    @Bean
    public DataSource securityDataSource() {
        DriverManagerDataSource securityDataSource = new DriverManagerDataSource();
        securityDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        securityDataSource.setUrl(dbUrl);
        securityDataSource.setUsername(username);
        securityDataSource.setPassword(password);

        return securityDataSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(securityDataSource());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/swagger-ui.html#/**").permitAll()
                .antMatchers("/courses").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/api/lectures/**").authenticated()
                .antMatchers(HttpMethod.GET,"/courses/new").hasAnyRole(   "TEACHER")
                .antMatchers(HttpMethod.GET,"/courses/draft/**").hasAnyRole(   "TEACHER")
                .antMatchers(HttpMethod.GET,"/courses/**").authenticated()
                .antMatchers(HttpMethod.POST,"/courses/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/users").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/users/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/users/**").hasAnyRole("STUDENT", "TEACHER","ADMIN")
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticate")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .logoutSuccessUrl("/")
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied");
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource());
        return jdbcUserDetailsManager;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**","/webjars/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
