package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.CourseStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseStatusRepository extends JpaRepository<CourseStatus, Integer> {
    CourseStatus findAllByName(String name);
}
