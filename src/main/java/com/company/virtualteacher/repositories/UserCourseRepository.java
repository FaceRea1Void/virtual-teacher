package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.UserCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserCourseRepository extends JpaRepository<UserCourse, Integer> {
    List<UserCourse> findAllByStudentAndIsCompletedIsFalse(String student);

    List<UserCourse> findAllByStudentAndIsCompletedIsTrue(String student);

    UserCourse findByCourse_IdAndStudent(int courseId, String student);

    @Query(value = "select count(id) as count " +
            "from user_courses uc " +
            "where uc.course_id = :courseId and uc.is_completed is false " +
            "group by uc.course_id", nativeQuery = true)
    Integer countByCourseAndCompletedIsFalse(@Param("courseId") int courseId);

    int countUserCoursesByCourseId(int courseId);

    @Modifying
    @Transactional
    @Query(value = "update user_courses set is_completed = true where course_id = :courseId and student = :student", nativeQuery = true)
    void setCompleted(@Param("courseId") int courseId, @Param("student") String student);
}
