package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AssignmentRepository extends JpaRepository<Assignment, Integer> {
    Assignment findByLectureId(int lectureId);

    @Query("select a.fileName as fileName " +
            "from Assignment as a " +
            "where a.id = :id")
    String getFileNameById(@Param("id") int id);
}
