package com.company.virtualteacher.repositories;

import com.company.virtualteacher.models.CourseRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CourseRatingRepository extends JpaRepository<CourseRating, Integer> {
    @Query(value = "select avg(rating) as avg " +
            "from course_ratings cr " +
            "where cr.course_id = :courseId " +
            "group by cr.course_id", nativeQuery = true)
    Double getAverageRating(@Param("courseId") int courseId);

    CourseRating findByCourseIdAndUsername(int courseId, String username);
}
