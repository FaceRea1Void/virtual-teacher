package com.company.virtualteacher.models;

public class UserLectureProgressDTO {
    private boolean videoWatched;
    private boolean homeworkSubmitted;

    public UserLectureProgressDTO() {
    }

    public UserLectureProgressDTO(boolean videoWatched, boolean homeworkSubmitted) {
        this.videoWatched = videoWatched;
        this.homeworkSubmitted = homeworkSubmitted;
    }

    public boolean isVideoWatched() {
        return videoWatched;
    }

    public void setVideoWatched(boolean videoWatched) {
        this.videoWatched = videoWatched;
    }

    public boolean isHomeworkSubmitted() {
        return homeworkSubmitted;
    }

    public void setHomeworkSubmitted(boolean homeworkSubmitted) {
        this.homeworkSubmitted = homeworkSubmitted;
    }
}
