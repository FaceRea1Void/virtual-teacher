package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_courses")
public class UserCourse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "student")
    @NotNull
    @Size(min = 3, max = 15, message = "Student name should be between 3 and 25 characters.")
    private String student;

    @ManyToOne
    @JoinColumn(name = "course_id")
    @NotNull
    private Course course;

    @Column(name = "is_completed")
    @NotNull
    private Boolean isCompleted;

    public UserCourse() {
    }

    public UserCourse(int id, String student, Course course, Boolean isCompleted) {
        this.id = id;
        this.student = student;
        this.course = course;
        this.isCompleted = isCompleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course courseId) {
        this.course = courseId;
    }

    public Boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(Boolean completed) {
        isCompleted = completed;
    }
}
