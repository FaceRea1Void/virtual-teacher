package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="course_ratings")
public class CourseRating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "course_id")
    @NotNull
    private int courseId;

    @Column(name = "username")
    @NotNull
    @Size(min = 5, max = 15, message = "Username should be between 5 and 25 characters.")
    private String username;

    @Column(name = "rating")
    @NotNull
    @Min(1)
    @Max(5)
    private int rating;

    public CourseRating() {
    }

    public CourseRating(int id, int courseId, String username, int rating) {
        this.id = id;
        this.courseId = courseId;
        this.username = username;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
