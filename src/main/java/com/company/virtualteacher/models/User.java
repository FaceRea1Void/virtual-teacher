package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "username")
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9_.]{3,15}$", message = "Username should be between 3 and 15 alphanumeric characters.")
    private String username;

    @Column(name = "email")
    @NotNull
    @Email(message="Please provide a valid email address.")
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "Please provide a valid email address.",
            flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.UNICODE_CASE})
    private String email;

    @Column(name = "first_name")
    @Size(min = 0, max = 25, message = "First name should be up to 25 characters.")
    private String firstName;

    @Column(name = "middle_name")
    @Size(min = 0, max = 25, message = "Middle name should be up to 25 characters.")
    private String middleName;

    @Column(name = "last_name")
    @Size(min = 0, max = 25, message = "Last name should be up to 25 characters.")
    private String lastName;

    @Column(name = "enabled")
    private Boolean enabled;

    @OneToOne
    @JoinColumn(name = "image_id")
    private UserImage image;

    public User() {
    }

    public User(int id, String username, String email, String firstName, String middleName,
                String lastName, Boolean enabled, UserImage image) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.enabled = enabled;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public UserImage getImage() {
        return image;
    }

    public void setImage(UserImage image) {
        this.image = image;
    }
}
