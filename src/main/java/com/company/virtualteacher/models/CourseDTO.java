package com.company.virtualteacher.models;

public class CourseDTO {
    private int id;
    private String title;
    private Category category;
    private String description;
    private String teacher;
    private double avgRating;
    private CourseImage image;

    public CourseDTO() {
    }

    public CourseDTO(int id, String title, Category category, String description,
                     String teacher, double avgRating, CourseImage image) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.description = description;
        this.teacher = teacher;
        this.avgRating = avgRating;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public CourseImage getImage() {
        return image;
    }

    public void setImage(CourseImage image) {
        this.image = image;
    }
}
