package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="lectures")
public class Lecture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "title")
    @NotNull
    @Size(min = 5, max = 50, message = "Title should be between 5 and 50 characters.")
    private String title;

    @Column(name = "description")
    @NotNull
    @Size(min = 5, max = 500, message = "Description should be between 5 and 500 characters.")
    private String description;

    @Column(name = "assignment_id")
    private Integer assignmentId;

    @Column(name = "course_id")
    @NotNull
    private int courseId;

    @Column(name = "prev_lecture_id")
    private Integer prevLectureId;

    @Column(name = "next_lecture_id")
    private Integer nextLectureId;

    @Column(name = "videoURL")
    @Size(min = 11, max = 11, message = "Invalid url.")
    private String videoURL;

    @Column(name = "is_active")
    @NotNull
    private Boolean isActive;

    public Lecture() {}

    public Lecture(int id, String title, String description, Integer assignmentId, int courseId, Integer nextLectureId, Integer prevLectureId, String videoURL, Boolean isActive) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.assignmentId = assignmentId;
        this.courseId = courseId;
        this.nextLectureId = nextLectureId;
        this.prevLectureId = prevLectureId;
        this.videoURL = videoURL;
        this.isActive = isActive;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Integer assignmentId) {
        this.assignmentId = assignmentId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public Integer getNextLectureId() {
        return nextLectureId;
    }

    public void setNextLectureId(Integer nextLectureId) {
        this.nextLectureId = nextLectureId;
    }

    public Integer getPrevLectureId() {
        return prevLectureId;
    }

    public void setPrevLectureId(Integer prevLectureId) {
        this.prevLectureId = prevLectureId;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
