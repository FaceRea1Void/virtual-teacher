package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "course_images")
public class CourseImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "file_name")
    @NotNull
    @Size(min = 5, max = 100, message = "File name should be between 5 and 100 characters.")
    private String fileName;

    @Column(name = "type")
    @NotNull
    @Size(min = 5, max = 15, message = "File extension should be between 5 and 15 characters.")
    private String fileType;

    @Column(name = "image")
    private byte[] image;

    @Column(name = "course_id")
    @NotNull
    private int courseId;

    public CourseImage() {
    }

    public CourseImage(String fileName, String fileType, byte[] image, int courseId) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.image = image.clone();
        this.courseId = courseId;
    }

    public CourseImage(int id, String fileName, String fileType, byte[] image, int courseId) {
        this.id = id;
        this.fileName = fileName;
        this.fileType = fileType;
        this.image = image.clone();
        this.courseId = courseId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getImage() {
        return image.clone();
    }

    public void setImage(byte[] image) {
        this.image = image.clone();
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }
}
