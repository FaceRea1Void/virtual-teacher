package com.company.virtualteacher.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;


import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTO {
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9_.]{3,15}$", message = "Username should be between 3 and 15 alphanumeric characters.")
    private String username;

    @Size(min = 8, max = 64, message = "The password should be between 8 and 64 characters.")
    private String password;

    @Email(message="Please provide a valid email address.")
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "Please provide a valid email address.",
            flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.UNICODE_CASE})
    private String email;

    @Size(min = 25, max = 500, message = "The resume should be between 25 and 500 characters.")
    private String resume;


    public UserDTO() {
    }

    public UserDTO(String username, String password, String email, String resume) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.resume = resume;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }
}
