package com.company.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "pending_approvals")
public class PendingApprovals {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private int id;

    @Column(name = "username")
    @NotNull
    @Size(min = 5, max = 15, message = "Username should be between 5 and 15 characters.")
    private String username;

    @Column(name = "resume")
    @NotNull
    @Size(min = 5, max = 500, message = "Resume should be between 5 and 500 characters.")
    private String description;

    public PendingApprovals() {
    }

    public PendingApprovals(String username, String description) {
        this.username = username;
        this.description = description;
    }

    public PendingApprovals(int id, String username, String description) {
        this.id = id;
        this.username = username;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
