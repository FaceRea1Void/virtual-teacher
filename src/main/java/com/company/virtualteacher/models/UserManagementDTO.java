package com.company.virtualteacher.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserManagementDTO {
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9_.]{3,15}$", message = "Username should be between 3 and 15 alphanumeric characters.")
    private String username;

    private UserImage image;

    public UserManagementDTO() {
    }

    public UserManagementDTO(String username, UserImage image) {
        this.username = username;
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserImage getImage() {
        return image;
    }

    public void setImage(UserImage image) {
        this.image = image;
    }
}
