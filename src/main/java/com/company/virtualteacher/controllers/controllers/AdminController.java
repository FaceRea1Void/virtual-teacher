package com.company.virtualteacher.controllers.controllers;

import com.company.virtualteacher.models.User;
import com.company.virtualteacher.services.interfaces.CourseService;
import com.company.virtualteacher.services.interfaces.UserPresentationService;
import com.company.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Base64;

@Controller
public class AdminController {
    private UserService userService;
    private UserPresentationService userPresentationService;
    private CourseService courseService;


    @Autowired
    public AdminController(UserService userService, UserPresentationService userPresentationService,
                           CourseService courseService) {
        this.userService = userService;
        this.userPresentationService = userPresentationService;
        this.courseService = courseService;
    }

    @GetMapping("/admin")
    public String showRegisterPage(Model model) {
        String username = ((org.springframework.security.core.userdetails.User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userService.getDetails(username);
        model.addAttribute("user", user);
        if (user.getImage() != null) {
            model.addAttribute("image", Base64.getEncoder().encodeToString(user.getImage().getImage()));
        }

        model.addAttribute("teachersCount", userPresentationService.getTeachers().size())
                .addAttribute("studentsCount", userPresentationService.getStudents().size())
                .addAttribute("coursesCount", courseService.getCourses());
        return "admin";
    }
}
