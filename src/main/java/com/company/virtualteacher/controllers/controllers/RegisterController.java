package com.company.virtualteacher.controllers.controllers;

import com.company.virtualteacher.models.UserDTO;
import com.company.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@Controller
@Valid
public class RegisterController {
    private UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegisterController(UserService userService, PasswordEncoder passwordEncode) {
        this.userService = userService;
        this.passwordEncoder = passwordEncode;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDTO());
        List principal = (List) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if(((SimpleGrantedAuthority) principal.get(0)).getAuthority().contains("ROLE_ANONYMOUS")){
            return "register";
        }else if(((SimpleGrantedAuthority) principal.get(0)).getAuthority().contains("ROLE_ADMIN")){
            return "redirect:/admin";
        }else{
            return "redirect:/";
        }
    }

    @GetMapping("/register/teacher")
    public String showTeacherRegisterPage(Model model) {
        model.addAttribute("user", new UserDTO());
        List principal = (List) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if(((SimpleGrantedAuthority) principal.get(0)).getAuthority().contains("ROLE_ANONYMOUS")){
            return "register-teacher";
        }else if(((SimpleGrantedAuthority) principal.get(0)).getAuthority().contains("ROLE_ADMIN")){
            return "redirect:/admin";
        }else{
            return "redirect:/";
        }
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDTO user, BindingResult bindingResult,
                               HttpServletRequest request, Model model) throws ServletException {

        if (bindingResult.hasErrors()) {
            if (bindingResult.getFieldError("username") != null) {
                model.addAttribute("error_username", Objects.requireNonNull(bindingResult
                        .getFieldError("username")).getDefaultMessage());
            }

            if (bindingResult.getFieldError("email") != null) {
                model.addAttribute("error_email", Objects.requireNonNull(bindingResult
                        .getFieldError("email")).getDefaultMessage());
            }

            if (bindingResult.getFieldError("password") != null) {
                model.addAttribute("error_password", Objects.requireNonNull(bindingResult
                        .getFieldError("password")).getDefaultMessage());
            }

            user.setPassword(null);
            model.addAttribute("user", user);
            if(user.getResume() == null) {
                return "register";
            }else{
                return "teacher";
            }
        }


        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_STUDENT");
        User newUser =
                new User(
                        user.getUsername(), passwordEncoder.encode(user.getPassword()), authorities);


        try {
            userService.create(newUser, user.getResume(), user.getEmail());
        } catch (Exception e) {
            model.addAttribute("error_username_exists", e.getMessage());
            user.setPassword(null);
            model.addAttribute("user", user);
            if(user.getResume() == null) {
                return "register";
            }else{
                return "register-teacher";
            }
        }

        request.login(user.getUsername(), user.getPassword());
        return "redirect:/";
    }
}
