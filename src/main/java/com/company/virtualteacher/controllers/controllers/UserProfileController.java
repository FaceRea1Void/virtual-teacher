package com.company.virtualteacher.controllers.controllers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.User;
import com.company.virtualteacher.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;

@Controller
public class UserProfileController extends SecurityDetailsProvider {
    private UserService userService;

    @Autowired
    public UserProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/{username}")
    public String showUserPage(Model model, @PathVariable String username) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equals(username)) {
            return "redirect:/access-denied";
        }

        User user;
        try {
            user = userService.getDetails(username);
            model.addAttribute("user", user);
            if (user.getImage() != null) {
                model.addAttribute("image", Base64.getEncoder().encodeToString(user.getImage().getImage()));
            }
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "userProfile";
    }
}
