package com.company.virtualteacher.controllers.controllers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseImage;
import com.company.virtualteacher.services.CourseStatus;
import com.company.virtualteacher.services.interfaces.CategoryService;
import com.company.virtualteacher.services.interfaces.CourseService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
public class CourseController extends SecurityDetailsProvider {
    private CourseService courseService;
    private CategoryService categoryService;

    @Autowired
    public CourseController(CourseService courseService, CategoryService categoryService) {
        this.courseService = courseService;
        this.categoryService = categoryService;
    }

    @GetMapping("courses/{courseId}")
    public String getCourse(@PathVariable int courseId, Model model) {
        Course course = getCourseById(courseId, CourseStatus.STATUS_ACTIVE);

        model.addAttribute("course", course);
        if (course.getImage() != null) {
            model.addAttribute("image", java.util.Base64.getEncoder().encodeToString(course.getImage().getImage()));
        }
        return "course";
    }

    @GetMapping("/courses/new")
    public String showCreatePage(Model model) {
        Course course = new Course();
        model.addAttribute("course", course);

        List<Category> categories = categoryService.getCategories();
        model.addAttribute("categories", categories);

        return "courseCreate";
    }

    @GetMapping("/courses/drafts/{courseId}")
    public String showDraftPage(Model model, @PathVariable Integer courseId) {
        Course course = getCourseById(courseId,  CourseStatus.STATUS_DRAFT);
        if (!course.getTeacher().equals(getLoggedUsername())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Course not found.");
        }
        CourseImage courseImage = course.getImage();
        if (courseImage != null) {
            model.addAttribute("picture", Base64.encodeBase64String(courseImage.getImage()));
        }

        model.addAttribute("course", course);

        List<Category> categories = categoryService.getCategories();
        model.addAttribute("categories", categories);

        return "courseCreate";
    }

    private Course getCourseById(int id, int statusId) {
        try {
            return courseService.getByIdAndStatus(id, statusId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
