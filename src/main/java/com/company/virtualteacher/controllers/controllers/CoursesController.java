package com.company.virtualteacher.controllers.controllers;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.services.interfaces.CategoryService;
import com.company.virtualteacher.services.interfaces.UserPresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Controller
public class CoursesController {
    private CategoryService categoryService;
    private UserPresentationService userPresentationService;
    private String categoryName;

    @Autowired
    public CoursesController(CategoryService categoryService,
                             UserPresentationService userPresentationService) {
        this.categoryService = categoryService;
        this.userPresentationService = userPresentationService;
    }

    @PostMapping("/courses")
    public void coursesFilterByCategory(@Valid @RequestBody Category category) {
        categoryName = category.getName();
    }

    @GetMapping("/courses")
    public String allCourses(Model model) {
        if (categoryName != null) {
            model.addAttribute("categoryFilter", categoryName);
            categoryName = null;
        }

        model.addAttribute("teachers", userPresentationService.getTeachers());
        model.addAttribute("categories", categoryService.getCategories());

        return "courses";
    }
}
