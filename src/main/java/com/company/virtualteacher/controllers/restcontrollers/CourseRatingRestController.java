package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.CourseRating;
import com.company.virtualteacher.services.interfaces.CourseRatingService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/courses")
public class CourseRatingRestController extends SecurityDetailsProvider {
    private CourseRatingService courseRatingService;

    public CourseRatingRestController(CourseRatingService courseRatingService) {
        this.courseRatingService = courseRatingService;
    }

    @PutMapping("/{courseId}/rate")
    public CourseRating rateCourse(@Valid @RequestBody CourseRating courseRating, @PathVariable int courseId) {
        if (courseRating.getCourseId() != courseId) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Page not found.");
        }
        try {
            courseRatingService.add(courseRating, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return courseRating;
    }

    @GetMapping("/{courseId}/rating")
    public int getUserRating(@PathVariable int courseId, @RequestParam String username) {
        return courseRatingService.getUserRating(courseId, username, getLoggedUsername());
    }
}
