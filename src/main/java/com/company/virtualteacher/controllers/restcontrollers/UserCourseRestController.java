package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.SecurityDetailsProvider;
import com.company.virtualteacher.models.UserCourse;
import com.company.virtualteacher.services.interfaces.UserCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/courses")
@Valid
public class UserCourseRestController extends SecurityDetailsProvider {
    private UserCourseService userCourseService;

    @Autowired
    public UserCourseRestController(UserCourseService userCourseService) {
        this.userCourseService = userCourseService;
    }

    @GetMapping("/{courseId}/enrolled")
    public boolean isStudentEnrolled(@PathVariable int courseId, @RequestParam String username) {
        return userCourseService.isStudentEnrolled(courseId, username);
    }

    @PostMapping("/{courseId}/enroll")
    public UserCourse enrollStudent(@Valid @RequestBody UserCourse userCourse, @PathVariable int courseId) {
        if (userCourse.getCourse().getId() != courseId) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Page not found.");
        }
        try {
            userCourseService.enroll(userCourse);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return userCourse;
    }

    @GetMapping("/{courseId}/enrolled/count")
    public int getEnrolledCount(@PathVariable int courseId) {
        try {
            return userCourseService.getEnrolledCount(courseId, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{courseId}/completed")
    public boolean isCompleted(@PathVariable int courseId, @RequestParam String username) {
        try {
            return userCourseService.isCompleted(courseId, username, getLoggedUsername());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
