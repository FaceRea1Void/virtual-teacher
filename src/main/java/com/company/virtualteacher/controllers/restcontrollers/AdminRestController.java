package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.models.CourseDTO;
import com.company.virtualteacher.models.PendingApprovals;
import com.company.virtualteacher.models.UserDTO;
import com.company.virtualteacher.models.UserManagementDTO;
import com.company.virtualteacher.services.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
@Valid
public class AdminRestController {
    private UserService userService;
    private UserPresentationService userPresentationService;
    private UserDetailsManager userDetailsManager;
    private CourseService courseService;
    private CoursePresentationService coursePresentationService;
    private UserCourseService userCourseService;
    private EmailNotificationService emailNotificationService;
    private PendingApprovalService pendingApprovalService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AdminRestController(UserService userService,
                               UserPresentationService userPresentationService,
                               UserDetailsManager userDetailsManager,
                               CourseService courseService,
                               CoursePresentationService coursePresentationService,
                               UserCourseService userCourseService,
                               EmailNotificationService emailNotificationService,
                               PendingApprovalService pendingApprovalService,
                               PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userPresentationService = userPresentationService;
        this.userDetailsManager = userDetailsManager;
        this.courseService = courseService;
        this.coursePresentationService = coursePresentationService;
        this.userCourseService = userCourseService;
        this.emailNotificationService = emailNotificationService;
        this.pendingApprovalService = pendingApprovalService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/users/students")
    public Page<UserManagementDTO> getStudents(@RequestParam(required = false) String username,
                                               @PageableDefault(size = 8) Pageable pageable) {
        return userPresentationService.getUsersListByUsernameAndRole(username, "ROLE_STUDENT", pageable);
    }

    @GetMapping("/users/teachers")
    public Page<UserManagementDTO> getTeachers(@RequestParam(required = false) String username,
                                               @PageableDefault(size = 8) Pageable pageable) {
        return userPresentationService.getUsersListByUsernameAndRole(username, "ROLE_TEACHER", pageable);
    }

    @GetMapping("/courses")
    public Page<CourseDTO> getCourses(@RequestParam(required = false) String title,
                                      @PageableDefault(size = 8) Pageable pageable) {
        return coursePresentationService.getCoursesByTitle(title, pageable);
    }

    @GetMapping("/courses/{courseId}/students")
    public int getStudentsCountByCourseId(@PathVariable int courseId) {
        return userCourseService.countUserCoursesByCourseId(courseId);
    }

    @GetMapping("/pendingapprovals")
    public Page<PendingApprovals> getPendingApprovals(@RequestParam(required = false) String username,
                                                      @PageableDefault(size = 5) Pageable pageable) {
        return pendingApprovalService.getPendingApprovals(username, pageable);
    }


    @PutMapping("/users/{username}/promote/teacher")
    public void promoteTeacher(@PathVariable String username) {
        try {
            org.springframework.security.core.userdetails.UserDetails oldUser = userDetailsManager.loadUserByUsername(username);

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_TEACHER");

            org.springframework.security.core.userdetails.User newUser =
                    new org.springframework.security.core.userdetails.User(username, oldUser.getPassword(), authorities);
            userService.changeRole(newUser);
            pendingApprovalService.approve(username);
            emailNotificationService.sendEmail(username);

        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (MessagingException e) {
            e.getMessage();
        }
    }

    @PutMapping("/users/{username}/promote/admin/")
    public void promoteAdmin(@PathVariable String username) {
        try {
            org.springframework.security.core.userdetails.UserDetails oldUser = userDetailsManager.loadUserByUsername(username);

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_ADMIN");

            org.springframework.security.core.userdetails.User newUser =
                    new org.springframework.security.core.userdetails.User(username, oldUser.getPassword(), authorities);
            userService.changeRole(newUser);

        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public void createAdmin(@Valid @RequestBody UserDTO user) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_ADMIN");
        try {
            org.springframework.security.core.userdetails.User newUser =
                    new org.springframework.security.core.userdetails.User(
                            user.getUsername(), passwordEncoder.encode(user.getPassword()), authorities);

            userService.create(newUser, user.getResume(), user.getEmail());
        }catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/users/{username}")
    public void delete(@PathVariable String username) {
        try {
            List<GrantedAuthority> authorities = AuthorityUtils.NO_AUTHORITIES;

            org.springframework.security.core.userdetails.UserDetails newUser =
                    new org.springframework.security.core.userdetails.User(username, "", false, true, true, true, authorities);

            userService.delete(newUser);

        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/courses/{id}/delete")
    public void delete(@PathVariable int id) {
        try {
            courseService.deleteCourse(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
