package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.controllers.FileValidator;
import com.company.virtualteacher.models.CourseImage;
import com.company.virtualteacher.services.interfaces.CourseImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/courses")
@Valid
public class CourseImageRestController extends FileValidator {
    private static final String FILE_COULD_NOT_BE_STORED = "File could not be stored. Please try again later!";

    private CourseImageService courseImageService;

    @Autowired
    public CourseImageRestController(CourseImageService courseImageService) {
        this.courseImageService = courseImageService;
    }

    @PostMapping("/{courseId}/images/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable int courseId ,@RequestParam String username) {
        //authenticateUser(username, "ROLE_TEACHER");

        String fileName = validateFileAndName(file, "(.jpg|.jpeg|.png|.gif|.bmp)");
        CourseImage courseImage;

        try {
            courseImage = new CourseImage(file.getOriginalFilename(), file.getContentType(), file.getBytes(), courseId);
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, FILE_COULD_NOT_BE_STORED);
        }

        try {
            courseImageService.store(courseImage, username);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return fileName;
    }

    @GetMapping("/{courseId}/images")
    public ResponseEntity<Resource> get(@PathVariable int courseId) {
        try {
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(courseImageService.getByCourseId(courseId).getFileType()))
                    .body(new ByteArrayResource(courseImageService.getByCourseId(courseId).getImage()));
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
