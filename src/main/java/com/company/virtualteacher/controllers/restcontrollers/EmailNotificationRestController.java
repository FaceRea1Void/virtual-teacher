package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.services.interfaces.EmailNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/emailnotification")
@Valid
public class EmailNotificationRestController {
    private EmailNotificationService emailNotificationService;

    @Autowired
    public EmailNotificationRestController(EmailNotificationService emailNotificationService) {
        this.emailNotificationService = emailNotificationService;
    }

    @GetMapping("/teacherapproval/{username}")
    public void sendEmail(@PathVariable String username) {
        try {
            emailNotificationService.sendEmail(username);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (MessagingException e) {
            e.getMessage();
        }
    }
}
