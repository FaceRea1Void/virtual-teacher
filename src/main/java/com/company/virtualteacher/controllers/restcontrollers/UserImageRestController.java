package com.company.virtualteacher.controllers.restcontrollers;

import com.company.virtualteacher.models.UserImage;
import com.company.virtualteacher.services.UserImageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import com.company.virtualteacher.controllers.FileValidator;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/users/images")
@Valid
public class UserImageRestController extends FileValidator {
    private static final String FILE_COULD_NOT_BE_STORED = "File could not be stored. Please try again later!";

    private UserImageServiceImpl userImageService;

    @Autowired
    public UserImageRestController(UserImageServiceImpl userImageService) {
        this.userImageService = userImageService;
    }

    @PostMapping("/upload/{username}")
    public String uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String username) {
        validateFileAndName(file, "(.jpg|.jpeg|.png|.gif|.bmp)");
        UserImage image;

        try {
            image = new UserImage(file.getOriginalFilename(), username, file.getContentType(), file.getBytes());
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, FILE_COULD_NOT_BE_STORED);
        }

        try {
            userImageService.storeFile(image, username);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return file.getOriginalFilename();
    }

    @GetMapping("/{fileId}")
    public ResponseEntity<Resource> get(@PathVariable int fileId) {
        try {
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(userImageService.getFile(fileId).getFileType()))
                    .body(new ByteArrayResource(userImageService.getFile(fileId).getImage()));
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/download/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable int fileId) {
        UserImage dbFile = userImageService.getFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getImage()));
    }
}
