package com.company.virtualteacher.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

public class FileValidator extends SecurityDetailsProvider {
    private static final String INVALID_FILE = "Invalid file.";
    private static final String FILE_FORMAT_IS_NOT_SUPPORTED = "File format is not supported.";

    protected String validateFileAndName(MultipartFile file, String extensionRegex) {
        if(file == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, INVALID_FILE);
        }
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        if (!fileName.matches("([a-zA-Z0-9\\s_\\\\.\\-\\(\\):])+" + extensionRegex + "$")) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, FILE_FORMAT_IS_NOT_SUPPORTED);
        }

        return fileName;
    }
}
