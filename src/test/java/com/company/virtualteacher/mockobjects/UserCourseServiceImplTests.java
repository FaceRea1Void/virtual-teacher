package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.*;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.repositories.UserCourseRepository;
import com.company.virtualteacher.repositories.UserLectureRepository;
import com.company.virtualteacher.services.UserCourseServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.company.virtualteacher.services.CourseStatus.*;

@RunWith(MockitoJUnitRunner.class)
public class UserCourseServiceImplTests {
    @Mock
    private UserCourseRepository mockUserCourseRepository;

    @Mock
    private LectureRepository mockLectureRepository;

    @Mock
    private UserLectureRepository mockUserLectureRepository;

    @Mock
    private CourseRepository mockCourseRepository;

    @InjectMocks
    private UserCourseServiceImpl userCourseService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Category newCategory = new Category(1, "Category Name", image);
    Course course = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, STATUS_ACTIVE, null);
    UserCourse userCourse = new UserCourse(1, "didig", course, true);

    @Test
    public void isStudentEnrolled_Should_ReturnTrue_When_UserIsEnrolled(){
        // Arrange
        Mockito.when(mockUserCourseRepository.findByCourse_IdAndStudent(course.getId(), "didig"))
                .thenReturn(new UserCourse(1, "didig", course, true));

        // Act
        Boolean isEnrolled = userCourseService.isStudentEnrolled(1, "didig");

        // Assert
        Assert.assertEquals(true, isEnrolled);
    }

    @Test
    public void enroll_Should_EnrollStudent_When_StudentNotEnrolled(){
        // Arrange
        Lecture lecture = new Lecture(1, "Title", "Description", 1, course.getId(),
                null, null, null, true);
        List<Lecture> listLectures = new ArrayList<>();
        listLectures.add(lecture);

        Mockito.when(mockLectureRepository.findAllByCourseIdAndIsActiveTrue(userCourse.getCourse().getId()))
                .thenReturn(listLectures);

        // Act
        userCourseService.enroll(userCourse);

        // Assert
        Mockito.verify(mockUserCourseRepository, Mockito.times(1)).save(userCourse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void enroll_Should_ThrowException_When_StudentIsEnrolled(){
        // Arrange
        Mockito.when(mockUserCourseRepository.findByCourse_IdAndStudent(userCourse.getCourse().getId(), userCourse.getStudent()))
                .thenReturn(userCourse);

        // Act
        userCourseService.enroll(userCourse);
    }

    @Test
    public void getEnrolledCount_Should_ReturnCountOfEnrolledStudentsByCourse_When_CourseHasEnrolledStudents(){
        // Arrange
        Mockito.when(mockUserCourseRepository.countByCourseAndCompletedIsFalse(5))
                .thenReturn(3);
        Mockito.when(mockCourseRepository.findByIdAndStatus(5, STATUS_ACTIVE))
                .thenReturn(course);
        // Act
        int result = userCourseService.getEnrolledCount(5, "vasko");

        // Assert
        Assert.assertEquals(result, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getEnrolledCount_Should_ThrowException_When_CourseNotFound(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(5, STATUS_ACTIVE))
                .thenReturn(null);

        // Act, Assert
        userCourseService.getEnrolledCount(5, "vasko");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getEnrolledCount_Should_ThrowException_When_UserNotCourseTeacher(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(5, STATUS_ACTIVE))
                .thenReturn(course);

        // Act, Assert
        userCourseService.getEnrolledCount(5, "didig");
    }

    @Test
    public void countUserCoursesByCourseId_Should_ReturnCountOfUserCourses_When_UserHasEnrolledCourses(){
        // Arrange
        Mockito.when(mockUserCourseRepository.countUserCoursesByCourseId(5))
                .thenReturn(3);

        // Act
        int result = userCourseService.countUserCoursesByCourseId(5);

        // Assert
        Assert.assertEquals(result, 3);
    }

    @Test
    public void isCompleted_Should_ReturnTrue_When_CourseIsCompletedByStudent(){
        // Arrange
        Mockito.when(mockUserCourseRepository.findByCourse_IdAndStudent(userCourse.getId(), userCourse.getStudent()))
                .thenReturn(userCourse);

        // Act
        boolean result = userCourseService.isCompleted(userCourse.getId(), userCourse.getStudent(), "didig");

        // Assert
        Assert.assertEquals(result, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isCompleted_Should_ThrowException_When_UsernameNotMatch(){
        // Arrange, Act, Assert
        userCourseService.isCompleted(userCourse.getId(), userCourse.getStudent(), "vasko");
    }
}
