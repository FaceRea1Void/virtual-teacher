package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.User;
import com.company.virtualteacher.models.UserImage;
import com.company.virtualteacher.repositories.UserImageRepository;
import com.company.virtualteacher.repositories.UserRepository;
import com.company.virtualteacher.services.UserImageServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserImageServiceImplTests {
    @Mock
    private UserImageRepository mockUserImageRepository;

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private UserImageServiceImpl userImageService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    UserImage newImage = new UserImage("user_image", "student", "Type", image);

    @Test
    public void storeFile_Should_CallRepositoryCreate_When_UserAndFileExist() {
        // Arrange
        Mockito.when(mockUserRepository.findUserByUsername("student"))
                .thenReturn(new User(1, "student", "user@email.com", "First Name",
                        "Middle Name",
                        "Last Name", true,  null));

        Mockito.when(mockUserImageRepository.save(newImage))
                .thenReturn(newImage);

        // Act
        userImageService.storeFile(newImage, "student");

        // Assert
        Mockito.verify(mockUserImageRepository, Mockito.times(1)).save(newImage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storeFile_Should_ThrowException_When_UserNotExists() {
        // Arrange

        // Act
        userImageService.storeFile(newImage, "student");

        // Assert
        Mockito.verify(mockUserImageRepository, Mockito.never()).save(newImage);
    }

    @Test
    public void getFile_Should_ReturnUserImage_When_IdExists() {
        // Arrange
        Mockito.when(mockUserImageRepository.findById(1))
                .thenReturn(java.util.Optional.of(
                        new UserImage("user_image", "student", "Type", image)));

        // Act
        UserImage image = userImageService.getFile(1);

        // Assert
        Assert.assertEquals("student", image.getUsername());
        Assert.assertEquals("user_image", image.getFileName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFile_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        userImageService.getFile(1);

        // Assert
        Mockito.verify(mockUserImageRepository, Mockito.never()).findById(1);
    }
}
