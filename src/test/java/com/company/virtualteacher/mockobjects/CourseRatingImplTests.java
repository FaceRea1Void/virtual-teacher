package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseRating;
import com.company.virtualteacher.models.UserCourse;
import com.company.virtualteacher.repositories.CourseRatingRepository;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.UserCourseRepository;
import com.company.virtualteacher.services.CourseRatingServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CourseRatingImplTests {
    @Mock
    private CourseRatingRepository mockCourseRatingRepository;

    @Mock
    private CourseRepository mockCourseRepository;

    @Mock
    private UserCourseRepository mockUserCourseRepository;

    @InjectMocks
    private CourseRatingServiceImpl courseRatingService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    CourseRating courseRating = new CourseRating(1, 1, "TestUser", 5);
    Category newCategory = new Category(1, "Category Name", image);
    Course course = new Course(1, "Test Title", newCategory, "Description", "Program", "TestTeacher", 0.0, 1, null);
    UserCourse userCourse = new UserCourse(1, "TestUser", course, false);

    @Test(expected = IllegalArgumentException.class)
    public void add_Should_ThrowException_When_CourseNotExist() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseRating.getCourseId(), 1))
                .thenReturn(null);

        // Act
        courseRatingService.add(courseRating, "TestUser");

        // Assert
        Mockito.verify(mockCourseRatingRepository, Mockito.never()).save(courseRating);
        Mockito.verify(mockCourseRepository, Mockito.never()).save(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_Should_ThrowException_When_UserCourseNotExist() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseRating.getCourseId(), 1))
                .thenReturn(course);
        Mockito.when(mockUserCourseRepository.findByCourse_IdAndStudent(courseRating.getCourseId(), courseRating.getUsername()))
                .thenReturn(null);

        // Act
        courseRatingService.add(courseRating, "TestUser");

        // Assert
        Mockito.verify(mockCourseRatingRepository, Mockito.never()).save(courseRating);
        Mockito.verify(mockCourseRepository, Mockito.never()).save(course);
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_Should_ThrowException_When_UserCourseNotCompleted() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseRating.getCourseId(), 1))
                .thenReturn(course);
        Mockito.when(mockUserCourseRepository.findByCourse_IdAndStudent(courseRating.getCourseId(), courseRating.getUsername()))
                .thenReturn(userCourse);

        // Act
        courseRatingService.add(courseRating, "TestUser");

        // Assert
        Mockito.verify(mockCourseRatingRepository, Mockito.never()).save(courseRating);
        Mockito.verify(mockCourseRepository, Mockito.never()).save(course);
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_Should_ThrowException_When_CourseIsRatedByUser() {
        // Arrange
        userCourse.setCompleted(true);
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseRating.getCourseId(), 1))
                .thenReturn(course);
        Mockito.when(mockUserCourseRepository.findByCourse_IdAndStudent(courseRating.getCourseId(), courseRating.getUsername()))
                .thenReturn(userCourse);
        Mockito.when(mockCourseRatingRepository.findByCourseIdAndUsername(courseRating.getCourseId(), courseRating.getUsername()))
                .thenReturn(courseRating);

        // Act
        courseRatingService.add(courseRating, "TestUser");

        // Assert
        Mockito.verify(mockCourseRatingRepository, Mockito.never()).save(courseRating);
        Mockito.verify(mockCourseRepository, Mockito.never()).save(course);
    }

    @Test
    public void add_Should_CallRepositoryAdd_When_CourseNotRatedByUser() {
        // Arrange
        userCourse.setCompleted(true);
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseRating.getCourseId(), 1))
                .thenReturn(course);
        Mockito.when(mockUserCourseRepository.findByCourse_IdAndStudent(courseRating.getCourseId(), courseRating.getUsername()))
                .thenReturn(userCourse);

        // Act
        courseRatingService.add(courseRating, "TestUser");

        // Assert
        Mockito.verify(mockCourseRatingRepository, Mockito.times(1)).save(courseRating);
        Mockito.verify(mockCourseRepository, Mockito.times(1)).save(course);
    }

    @Test
    public void getUserRating_Should_ReturnZero_UserRatingNotExist() {
        // Arrange
        Mockito.when(mockCourseRatingRepository.findByCourseIdAndUsername(1, "TestUser"))
                .thenReturn(null);

        // Act
        int result = courseRatingService.getUserRating(1, "TestUser", "TestUser");

        // Assert
        Assert.assertEquals(0, result);
    }

    @Test
    public void getUserRating_Should_ReturnUserRatingValue_UserRatingExist() {
        // Arrange
        Mockito.when(mockCourseRatingRepository.findByCourseIdAndUsername(1, "TestUser"))
                .thenReturn(courseRating);

        // Act
        int result = courseRatingService.getUserRating(1, "TestUser", "TestUser");

        // Assert
        Assert.assertEquals(courseRating.getRating(), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public  void getUserRating_Should_ThtrowException_When_LoggedUserNotMatchesTheUsername(){
        // Arrange

        // Act
        courseRatingService.getUserRating(1, "vasko", "didi");

        // Assert
        Mockito.verify(mockCourseRatingRepository, Mockito.never()).findByCourseIdAndUsername(1, "vasko");
    }
}
