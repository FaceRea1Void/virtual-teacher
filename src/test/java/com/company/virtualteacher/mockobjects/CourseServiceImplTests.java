package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.*;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.services.CourseServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.company.virtualteacher.services.CourseStatus.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceImplTests {
    @Mock
    private CourseRepository mockCourseRepository;

    @Mock
    private LectureRepository mockLectureRepository;

    @InjectMocks
    private CourseServiceImpl courseService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Category newCategory = new Category(1, "Category Name", image);

    Course courseDraft  = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, STATUS_DRAFT, null);
    Course courseActive  = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, STATUS_ACTIVE, null);


    @Test
    public void create_Should_CallRepositoryCreate_When_CourseNameIsUnique() {
        // Arrange

        // Act
        courseService.create(courseDraft, "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.times(1)).save(courseDraft);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_CourseTeacherNotMatchUsername() {
        // Arrange

        // Act
        courseService.create(courseDraft, "didig");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseDraft);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_CourseNameIsNotUnique() {
        // Arrange
        Mockito.when(mockCourseRepository.findCourseByTitle(any()))
                .thenReturn(new Course(2, "Course Title", newCategory, "Description",
                        "Program", "didig", 1, STATUS_DRAFT, null));

        // Act
        courseService.create(courseDraft, "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseDraft);
    }

    @Test
    public void getByTitle_Should_ReturnCourse_When_CourseWithTitleExists() {
        // Arrange
        Mockito.when(mockCourseRepository.findCourseByTitle(courseActive.getTitle()))
                .thenReturn(courseActive);

        // Act
        Course result = courseService.getByTitle(courseActive.getTitle());

        // Assert
        Assert.assertEquals("Course Title", result.getTitle());
    }


    @Test
    public void updateDraft_Should_UpdateCourse_When_CourseIsInStatusDraft() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        // Act
        courseService.updateDraft(courseDraft, "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.times(1)).save(courseDraft);
    }


    @Test(expected = IllegalArgumentException.class)
    public void updateDraft_Should_ThrowException_When_CourseIsNotInStatusDraft() {
        // Arrange

        // Act
        courseService.updateDraft(courseDraft, "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseDraft);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateDraft_Should_ThrowException_When_CourseTeacherNotMatchUsername() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);
        // Act
        courseService.updateDraft(courseDraft, "didig");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseDraft);
    }

    @Test
    public void update_Should_UpdateCourse_When_CourseIsInStatusActive() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        // Act
        courseService.update(courseActive, "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.times(1)).save(courseActive);
    }


    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_CourseIsNotInStatusActive() {
        // Arrange

        // Act
        courseService.update(courseActive, "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseActive);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_CourseTeacherNotMatchUsername() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);
        // Act
        courseService.update(courseActive, "didig");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseActive);
    }

    @Test(expected = IllegalArgumentException.class)
    public void activateCourse_Should_ThrowException_When_CourseDoesNotHaveImage() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        // Act
        courseService.activateCourse(courseDraft.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).activateCourse(courseDraft.getId());
    }

    @Test
    public void activateCourse_Should_ChangeStatus_When_CourseIsSubmitted() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        Lecture lecture = new Lecture(1, "Lecture title", "Lecture Description", 1, courseDraft.getId(), null, null, "testurl", true);
        Mockito.when(mockLectureRepository.findAllByCourseIdAndIsActiveTrue(courseDraft.getId()))
                .thenReturn(Arrays.asList(lecture));
        CourseImage courseImage = new CourseImage(1, "ImageName", "fileType", image, courseDraft.getId());
        courseDraft.setImage(courseImage);
        // Act
        courseService.activateCourse(courseDraft.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.times(1)).activateCourse(courseDraft.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void activateCourse_Should_ThrowException_When_CourseImageIsNotProvided() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        // Act
        courseService.activateCourse(courseActive.getId(), "vasko");
    }

    @Test(expected = IllegalArgumentException.class)
    public void activateCourse_Should_ThrowException_When_CourseIsNotInStatusDraft() {
        // Arrange

        // Act
        courseService.activateCourse(courseActive.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseActive);
    }

    @Test(expected = IllegalArgumentException.class)
    public void activateCourse_Should_ThrowException_When_CourseTeacherNotMatchUsername() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);
        // Act
        courseService.activateCourse(courseActive.getId(), "didig");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseActive);
    }

    @Test(expected = IllegalArgumentException.class)
    public void activateCourse_Should_ThrowException_When_CourseHasNoLectures() {
        // Arrange
        CourseImage courseImage = new CourseImage(1, "ImageName", "fileType", image, courseDraft.getId());
        courseDraft.setImage(courseImage);
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        // Act
        courseService.activateCourse(courseActive.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseActive);
    }


    @Test(expected = IllegalArgumentException.class)
    public void activateCourse_Should_ThrowException_When_LectureInCourseHasNoAttachment() {
        // Arrange
        CourseImage courseImage = new CourseImage(1, "ImageName", "fileType", image, courseDraft.getId());
        courseDraft.setImage(courseImage);
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseDraft.getId(), courseDraft.getStatus()))
                .thenReturn(courseDraft);

        List<Lecture> lectureList = new ArrayList<>();
        Lecture newLecture = new Lecture(1, "Title", "Description", null, 1, null, null, "video", false);
        lectureList.add(newLecture);
        Mockito.when(mockLectureRepository.findAllByCourseIdAndIsActiveTrue(courseDraft.getId()))
                .thenReturn(lectureList);

        // Act
        courseService.activateCourse(courseActive.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).save(courseActive);
    }

    @Test
    public void getByIdAndStatus_Should_ReturnCourse_When_IdAndStatusAreValid(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(courseActive.getId(), courseActive.getStatus()))
                .thenReturn(courseActive);

        // Act
        Course result = courseService.getByIdAndStatus(courseActive.getId(), courseActive.getStatus());

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals(1, result.getStatus());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByIdAndStatus_Should_ThrowException_When_CourseWithIdAndStatusNotExists(){
        // Arrange

        // Act & // Assert
        courseService.getByIdAndStatus(courseActive.getId(), courseActive.getStatus());
    }


    @Test
    public void deleteCourse_Should_DeleteCourse_When_CourseIsNotSubmitted(){
        // Arrange
        Mockito.when(mockCourseRepository.findById(courseDraft.getId()))
                .thenReturn(Optional.of(courseDraft));

        // Act
        courseService.deleteCourse(courseDraft.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.times(1)).deleteCourse(courseDraft.getId());
    }


    @Test(expected = IllegalArgumentException.class)
    public void deleteCourse_Should_ThrowException_When_CourseNotFound(){
        // Arrange

        // Act
        courseService.deleteCourse(courseDraft.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).deleteCourse(courseDraft.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteCourse_Should_ThrowException_When_CourseTeacherNotMatchUsername(){
        // Arrange
        Mockito.when(mockCourseRepository.findById(courseDraft.getId()))
                .thenReturn(Optional.of(courseDraft));

        // Act
        courseService.deleteCourse(courseDraft.getId(), "didig");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).deleteCourse(courseDraft.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteCourse_Should_ThrowException_When_CourseNotDraftOrActive(){
        // Arrange
        Mockito.when(mockCourseRepository.findById(courseDraft.getId()))
                .thenReturn(Optional.of(new Course(1, "Course Title", newCategory, "Description",
                        "Program", "vasko", 1, STATUS_DELETED, null)));

        // Act
        courseService.deleteCourse(courseDraft.getId(), "vasko");

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.never()).deleteCourse(courseDraft.getId());
    }

    @Test
    public void deleteCourse_Should_DeleteCourse_When_CourseIsSubmitted(){
        // Arrange

        // Act
        courseService.deleteCourse(1);

        // Assert
        Mockito.verify(mockCourseRepository, Mockito.times(1)).deleteCourse(1);
    }

    @Test
    public void getCourses_Should_ReturnCountOfCourses_When_CoursesAreInStatusActive(){
        // Arrange
        Mockito.when(mockCourseRepository.countCoursesByStatus(STATUS_ACTIVE))
                .thenReturn(3);

        // Act
        int result = courseService.getCourses();

        // Assert
        Assert.assertEquals(result, 3);
    }
}
