package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseImage;
import com.company.virtualteacher.models.CourseStatus;
import com.company.virtualteacher.repositories.CourseImageRepository;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.services.CourseImageServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CourseImageServiceImplTests {
    @Mock
    private CourseRepository mockCourseRepository;

    @Mock
    private CourseImageRepository mockCourseImageRepository;

    @InjectMocks
    private CourseImageServiceImpl courseImageService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Category newCategory = new Category(1, "Category Name", image);
    CourseStatus statusDraft = new CourseStatus(2, "Draft");
    CourseImage courseImage = new CourseImage("image", "type", image, 1);
    Course course = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, statusDraft.getId(), courseImage);


    @Test
    public void store_Should_StoreImage_When_CourseIsInStatusDraft() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(course.getId(), course.getStatus()))
                .thenReturn(course);

        // Act
        courseImageService.store(courseImage, course.getTeacher());

        // Assert
        Mockito.verify(mockCourseImageRepository, Mockito.times(1)).save(courseImage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void store_Should_ThrowException_When_CourseIsNotInStatusDraft() {
        // Arrange

        // Act
        courseImageService.store(courseImage, course.getTeacher());

        // Assert
        Mockito.verify(mockCourseImageRepository, Mockito.never()).save(courseImage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void store_Should_ThrowException_When_CourseNotBelongsToTeacher() {
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(course.getId(), course.getStatus()))
                .thenReturn(course);

        // Act
        courseImageService.store(courseImage, course.getTeacher() + "Test");

        // Assert
        Mockito.verify(mockCourseImageRepository, Mockito.never()).save(courseImage);
    }

    @Test
    public void getByCourseId_Should_ReturnCourse_When_CourseIdIsValid() {
        // Arrange
        Mockito.when(mockCourseImageRepository.findByCourseId(course.getId()))
                .thenReturn(courseImage);

        // Act
        CourseImage result = courseImageService.getByCourseId(course.getId());

        //
        Assert.assertEquals(result, courseImage);
    }
}
