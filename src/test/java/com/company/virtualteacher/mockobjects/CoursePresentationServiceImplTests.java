package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseDTO;
import com.company.virtualteacher.models.CourseStatus;
import com.company.virtualteacher.repositories.*;
import com.company.virtualteacher.services.CoursePresentationServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;

import java.util.ArrayList;
import java.util.List;

import static com.company.virtualteacher.services.CourseStatus.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CoursePresentationServiceImplTests {
    @Mock
    private CourseRepository mockCourseRepository;

    @Mock
    private CategoryRepository mockCategoryRepository;

    @InjectMocks
    private CoursePresentationServiceImpl coursePresentationService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Category newCategory = new Category(1, "Category Name", image);
    CourseStatus statusActive = new CourseStatus(1, "Active");
    Course courseActive  = new Course(1, "Course Title", newCategory, "Description",
            "Program", "vasko", 1, statusActive.getId(), null);

    @Test
    public void get_Should_ReturnPageWithCourses_When_CategoryFilterNotExists() {
        // Arrange
        List<Course> courses = new ArrayList<>();
        courses.add(courseActive);

        Page<Course> page = new PageImpl<>(courses);

        Mockito.when(mockCourseRepository.findAllByTitleContainingAndTeacherContainingAndStatus(
                "",
                "",
                1,
                null))
                .thenReturn(page);

        // Act
        Page<CourseDTO> result = coursePresentationService.getCourses(any(), any(), any(), any());

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }

    @Test
    public void get_Should_ReturnPageWithCourses_When_CategoryFilterExists() {
        // Arrange
        List<Course> courses = new ArrayList<>();
        courses.add(courseActive);

        Page<Course> page = new PageImpl<>(courses);

        Mockito.when(mockCourseRepository.findAllByTitleContainingAndTeacherContainingAndCategoryAndStatus(
                any(), any(), any(), anyInt(), any()))
                .thenReturn(page);

        // Act
        Page<CourseDTO> result = coursePresentationService.getCourses("Title", "didig", "Category", any());
        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }

    @Test
    public void topCategories_Should_ReturnTop3Categories_When_MethodIsCalled(){
        // Arrange
        ProjectionFactory factory = new SpelAwareProxyProjectionFactory();
        CategoryCoursesCount topCategoryOne = factory.createProjection(CategoryCoursesCount.class);
        topCategoryOne.setCategoryId(1);
        topCategoryOne.setCategoryName("Category");
        topCategoryOne.setCategoryImage(null);
        topCategoryOne.setCount(7);

        CategoryCoursesCount topCategoryTwo = factory.createProjection(CategoryCoursesCount.class);
        topCategoryTwo.setCategoryId(2);
        topCategoryTwo.setCategoryName("Category Two");
        topCategoryTwo.setCategoryImage(null);
        topCategoryTwo.setCount(6);

        CategoryCoursesCount topCategoryThree = factory.createProjection(CategoryCoursesCount.class);
        topCategoryThree.setCategoryId(3);
        topCategoryThree.setCategoryName("Category Three");
        topCategoryThree.setCategoryImage(null);
        topCategoryThree.setCount(6);

        CategoryCoursesCount topCategoryFour = factory.createProjection(CategoryCoursesCount.class);
        topCategoryFour.setCategoryId(4);
        topCategoryFour.setCategoryName("Category Four");
        topCategoryFour.setCategoryImage(null);
        topCategoryFour.setCount(6);

        List<CategoryCoursesCount> categories = new ArrayList<>();
        categories.add(topCategoryOne);
        categories.add(topCategoryTwo);
        categories.add(topCategoryThree);
        categories.add(topCategoryFour);

        Mockito.when(mockCourseRepository.getTopCategories())
                .thenReturn(categories);

        // Act
        List<CategoryCoursesCount> result = coursePresentationService.topCategories();

        // Assert
        Assert.assertEquals(4, categories.size());
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void countAllDistinctByCategory_Should_ReturnCategoryListGroupByCoursesCount_When_MethodIsCalled(){
        // Arrange
        ProjectionFactory factory = new SpelAwareProxyProjectionFactory();
        CategoryCoursesCount topCategoryOne = factory.createProjection(CategoryCoursesCount.class);
        topCategoryOne.setCategoryId(1);
        topCategoryOne.setCategoryName("Category");
        topCategoryOne.setCategoryImage(null);
        topCategoryOne.setCount(7);

        List<CategoryCoursesCount> categoriesList = new ArrayList<>();
        categoriesList.add(topCategoryOne);
        Page<CategoryCoursesCount> categories = new PageImpl<>(categoriesList);

        Mockito.when(coursePresentationService.countAllDistinctByCategory(any()))
                .thenReturn(categories);

        // Act
        Page<CategoryCoursesCount> result = coursePresentationService.countAllDistinctByCategory(any());

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }

    @Test
    public void getCoursesByTitle_Should_ReturnPageOfCourses_When_TitleIsNotSet(){
        // Arrange
        String title = null;
        List<Course> courses = new ArrayList<>();
        courses.add(courseActive);

        Page<Course> page = new PageImpl<>(courses);

        Mockito.when(mockCourseRepository.findAllByTitleContainingAndTeacherContainingAndStatus
                ("", "", STATUS_ACTIVE, null))
                .thenReturn(page);

        // Act
        Page<CourseDTO> result = coursePresentationService.getCoursesByTitle(title,  null);

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());

    }

    @Test
    public void getAllByTeacher_Should_ReturnListOfCoursesByTeacher_When_CourseStatusIsNotDeleted(){
        // Arrange
        List<Course> courseList = new ArrayList<>();
        courseList.add(courseActive);

        Mockito.when(mockCourseRepository.findByTeacherAndStatusNot("vasko",
                STATUS_DELETED))
                .thenReturn(courseList);

        // Act
        List<Course> result = coursePresentationService.getAllByTeacher("vasko");

        // Assert
        Assert.assertEquals(result.size(), 1);
    }
}
