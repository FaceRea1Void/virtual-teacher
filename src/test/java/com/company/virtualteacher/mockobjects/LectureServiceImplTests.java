package com.company.virtualteacher.mockobjects;

import com.company.virtualteacher.models.Category;
import com.company.virtualteacher.models.Course;
import com.company.virtualteacher.models.CourseStatus;
import com.company.virtualteacher.models.Lecture;
import com.company.virtualteacher.repositories.CourseRepository;
import com.company.virtualteacher.repositories.LectureRepository;
import com.company.virtualteacher.services.LectureServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class LectureServiceImplTests {
    @Mock
    private LectureRepository mockLectureRepository;

    @Mock
    private CourseRepository mockCourseRepository;

    @InjectMocks
    private LectureServiceImpl lectureService;

    byte[] image = new byte[]{(byte) 1, (byte) 2, (byte) 3};
    Lecture lecture = new Lecture(1, "Title", "Description", null, 1,
            null, null, null, true );
    Lecture lectureTwo = new Lecture(2, "Title Two", "Description", null, 1,
            null, null, null, true );
    Category newCategory = new Category(1, "Category Name", image);
    CourseStatus statusDraft = new CourseStatus(2, "Draft");
    Course course = new Course(1, "Title", newCategory, "Description", "Program",
            "vasko", 1, statusDraft.getId(), null);

    @Test
    public void getByTitle_Should_ReturnLecture_When_TitleExists(){
        // Arrange
        Mockito.when(mockLectureRepository.findLectureByTitle(lecture.getTitle()))
                .thenReturn(lecture);

        // Act
        Lecture result = lectureService.getByTitle(lecture.getTitle());

        // Assert
        Assert.assertEquals(result, lecture);
    }

    @Test
    public void getById_Should_ReturnLecture_When_IdExists(){
        // Arrange
        Mockito.when(mockLectureRepository.findById(lecture.getCourseId()))
                .thenReturn(Optional.of(lecture));

        // Act
        Lecture result = lectureService.getById(lecture.getId());

        // Assert
        Assert.assertEquals(result, lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ReturnLecture_When_IdNotExists(){
        // Arrange

        // Act
        lectureService.getById(lecture.getId());
    }

    @Test
    public void create_Should_CreateNewLecture_When_LectureAndCourseAreValid(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(), course.getStatus()))
                .thenReturn(course);

        // Act
        lectureService.create(lecture, "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.times(1)).save(lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_CourseIsNotInStatusDraft(){
        // Arrange

        // Act
        lectureService.create(lecture, "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_CourseBelongsToAnotherTeacher(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(), course.getStatus()))
                .thenReturn(course);

        // Act
        lectureService.create(lecture, "didig");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_LectureTitleIsDuplicated(){
        // Arrange
        lectureTwo = new Lecture(2, "Title", "Description", null, 1,
                null, null, null, true );

        List<Lecture> lectureList = new ArrayList<>();
        lectureList.add(lecture);
        lectureList.add(lectureTwo);

        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(), course.getStatus()))
                .thenReturn(course);

        Mockito.when(mockLectureRepository.findAllByCourseIdAndIsActiveTrue(lecture.getCourseId()))
                .thenReturn(lectureList);

        // Act
        lectureService.create(lecture, "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test
    public void create_Should_SetPreviousAndNextLecture_When_LectureAndCourseAreValid(){
        // Arrange
        List<Lecture> lectureList = new ArrayList<>();
        lectureList.add(lecture);

        Mockito.when(mockCourseRepository.findByIdAndStatus(lectureTwo.getCourseId(), course.getStatus()))
                .thenReturn(course);

        Mockito.when(mockLectureRepository.findAllByCourseIdAndIsActiveTrue(lectureTwo.getCourseId()))
                .thenReturn(lectureList);

        // Act
        lectureService.create(lectureTwo, "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.times(1)).save(lecture);
        Mockito.verify(mockLectureRepository, Mockito.times(1)).save(lectureTwo);
    }

    @Test
    public void update_Should_UpdateLectureContent_When_TitleAndCourseAreValid(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(), course.getStatus()))
                .thenReturn(course);

        Mockito.when(mockLectureRepository.findById(lecture.getId()))
                .thenReturn(Optional.of(lecture));

        // Act
        lectureService.update(lecture, lecture.getId(), "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.times(1)).save(lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_LectureNotExists(){
        // Arrange
        Mockito.when(mockCourseRepository.findByIdAndStatus(lecture.getCourseId(), course.getStatus()))
                .thenReturn(course);

        // Act
        lectureService.update(lecture, lecture.getId(), "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_LectureIdNotMatch(){
        // Arrange, Act
        lectureService.update(lecture, 2, "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_LectureIdsNotMatch(){
        // Arrange

        // Act
        lectureService.update(lecture, 2, "vasko");

        // Assert
        Mockito.verify(mockLectureRepository, Mockito.never()).save(lecture);
    }

    @Test
    public void countLecturesByCourseId_Should_ReturnCourseLecturesCount_When_CourseExists(){
        // Arrange
        Mockito.when(mockLectureRepository.countAllByCourseId(1))
                .thenReturn(3);

        // Act
        int result =lectureService.countLecturesByCourseId(1);

        // Assert
        Assert.assertEquals(result, 3);
    }

    @Test
    public void getLectureListByCourseId_Should_ReturnCourseLecturesList_When_CourseExists(){
        // Arrange
        List<Lecture> lectureList = new ArrayList<>();
        lectureList.add(lecture);
        lectureList.add(lectureTwo);

        Mockito.when(mockLectureRepository.findAllByCourseIdAndIsActiveTrue(course.getId()))
                .thenReturn(lectureList);

        // Act
        List<Lecture> result = lectureService.getLectureListByCourseId(course.getId());

        // Assert
        Assert.assertEquals(result.size(), 2);
    }
}
